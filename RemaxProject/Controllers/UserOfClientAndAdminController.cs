﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RemaxProject.Models;

namespace RemaxProject.Controllers
{
    public class UserOfClientAndAdminController : Controller
    {
        // GET: UserOfClientAndAdmin
        public ActionResult Index()
        {
            var userOfClientAndAdmin = new UserOfClientAndAdmin();
            userOfClientAndAdmin.ListUser = GetUsers();
            userOfClientAndAdmin.ListClient = GetClients();
            userOfClientAndAdmin.ListAdmin = GetAdmins();
            return View(userOfClientAndAdmin);
        }

        public List<User> GetUsers()
        {
            Context context = new Context();
            var listUser = context.Users.ToList();
            return listUser;
        }
        public List<Client> GetClients()
        {
            Context context = new Context();
            var ltstClient = context.Clients.ToList();
            return ltstClient;
        }
        public List<Admin> GetAdmins()
        {
            Context context = new Context();
            var lstAdmin = context.Admins.ToList();
            return lstAdmin;
        }


    }
}