﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RemaxProject.Models;

namespace RemaxProject.Controllers
{
    public class AdminController : Controller
    {
        /* list all admin id */
        public ActionResult Index()
        {
            var context = new Context();
            var listAdmin = context.Admins.ToList();
            return View(listAdmin);
        }

        /* create new admin table */
        [HttpPost]
        public ActionResult Create(Admin admin)
        {
            if (ModelState.IsValid)
            {
                Context context = new Context();
                context.Admins.Add(admin);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            return View();
        }
        [HttpPost]
        [ActionName("Create")]
        public ActionResult Create_Post(Admin admin)
        {
            if (ModelState.IsValid)
            {
                Context context = new Context();
                context.Admins.Add(admin);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        /* edit admin table */
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var context = new Context();
            var admin = context.Admins.Single(x => x.AdminId == id);
            return View(admin);
        }
        [HttpPost]
        public ActionResult Edit(Admin admin)
        {
            if (ModelState.IsValid)
            {
                var context = new Context();
                var updatedAdmin = context.Admins.Find(admin.AdminId);
                updatedAdmin.Region = admin.Region;
                updatedAdmin.City = admin.City;
                updatedAdmin.Price = admin.Price;
                updatedAdmin.Rooms = admin.Rooms;
                updatedAdmin.Bathrooms = admin.Bathrooms;
                updatedAdmin.Garage = admin.Garage;
                updatedAdmin.Pool = admin.Pool;
                updatedAdmin.Fireplace = admin.Fireplace;
                updatedAdmin.Image= admin.Image;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }

        }

        /* delete admin table */
        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (ModelState.IsValid)
            {
                var context = new Context();
                var admin = context.Admins.Find(id);
                context.Admins.Remove(admin);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }

        }
    }
}