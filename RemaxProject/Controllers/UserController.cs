﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RemaxProject.Models;

namespace RemaxProject.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            var context = new Context();
            var listUser = context.Users.ToList();
            return View(listUser);
        }

        public ActionResult Details(int id)
        {
            var context = new Context();
            var detailsUser = context.Users.Single(x => x.UserId == id);
            return View(detailsUser);
        }


        /* create new user table */
        [HttpPost]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                Context context = new Context();
                context.Users.Add(user);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            return View();
        }
        [HttpPost]
        [ActionName("Create")]
        public ActionResult Create_Post(User user)
        {
            if (ModelState.IsValid)
            {
                Context context = new Context();
                context.Users.Add(user);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        /* edit user table */
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var context = new Context();
            var user = context.Users.Single(x => x.UserId == id);
            return View(user);
        }
        [HttpPost]
        public ActionResult Edit(User user)
        {
            if (ModelState.IsValid)
            {
                var context = new Context();
                var updatedUser = context.Users.Find(user.UserId);
                updatedUser.Email = user.Email;
                updatedUser.Password = user.Password;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }

        }

        /* delete user table */
        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (ModelState.IsValid)
            {
                var context = new Context();
                var user = context.Users.Find(id);
                context.Users.Remove(user);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }

        }


    }
}