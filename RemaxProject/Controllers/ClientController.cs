﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RemaxProject.Models;

namespace RemaxProject.Controllers
{
    public class ClientController : Controller
    {
        /* list all client id */
        public ActionResult Index()
        {
            var context = new Context();
            var listClient = context.Clients.ToList();
            return View(listClient);
        }

        public ActionResult Details(int id)
        {
            var context = new Context();
            var client = context.Clients.Single(x => x.ClientId == id);
            return View(client);
        }


    }
}