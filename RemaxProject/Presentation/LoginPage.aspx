﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginPage.aspx.cs" Inherits="RemaxProject.Presentation.LoginPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>LoginPage</title>
     <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <style>
        .container {
          margin: auto;
          width: auto;
          border: 3px solid #73AD21;
          padding: 10px;
          font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
     }
    </style>
</head>
<body>
    <form id="form1" runat="server">

      <div class="container"> 
          <div class="form-group" >
            <label for="exampleInputEmail1">Email address</label>              <%-- <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--%>
              <asp:TextBox class="form-control" ID="TextBoxEmail" runat="server"></asp:TextBox>
              <%-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else. </small>--%>        
         </div>

          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>           <%-- <input  class="form-control" id="exampleInputPassword1">--%>
              <asp:TextBox class="form-control" type="password" ID="TextBoxPassword" runat="server"></asp:TextBox>
         </div>

          <div class="form-group form-check">
            <asp:Label ID="LabelMessage" runat="server" Text="Show message ..."></asp:Label>                 
          </div>
          
          <%--<button type="submit" class="btn btn-primary">Submit</button>--%>
          <asp:Button ID="ButtonSave" runat="server" Text="Submit" class="btn btn-primary" OnClick="ButtonSave_Click" />
     </div>

    </form>
</body>
</html>
