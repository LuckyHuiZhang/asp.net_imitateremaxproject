﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RemaxProject.Models;


namespace RemaxProject.Presentation
{
    public partial class ClientPage : System.Web.UI.Page
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {
            Admin admin = new Admin();
            GridViewListAdmin.DataSource = admin.ReadAdmin();
            GridViewListAdmin.DataBind();

            /* call GridView function to read data */
            FillGridView();

        }

        public void FillGridView()
        {
            Admin admin = new Admin();
            var dt = admin.ReadAdmin();
            GridViewListAdmin.DataSource = dt;
            GridViewListAdmin.DataBind();
        }


      
        protected void ButtonSave_Click1(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Admin admin = new Admin();
                admin.Region = RegionDrop.SelectedItem.Text;
                admin.City = CityDrop.SelectedItem.Text;
                admin.Price = Convert.ToInt32(PriceDrop.SelectedItem.Text);
                admin.Rooms = Convert.ToInt32(RoomDrop.SelectedItem.Text);
                admin.Bathrooms = Convert.ToInt32(BathroomDrop.SelectedItem.Text);
                admin.Garage = GarageDrop.SelectedItem.Text;
                admin.Pool = PoolDrop.SelectedItem.Text;
                admin.Fireplace = FireplaceDrop.SelectedItem.Text;
                /* call the function ReadCustomer from the DB */
                GridViewListAdmin.DataSource = admin.ReadAdmin();

                GridViewListAdmin.DataBind();

                /* call the function SaveCustomer from the DB */
                if (admin.SavAdmin(admin))
                {
                    StatusLbl.ForeColor = System.Drawing.Color.Green;
                    StatusLbl.Text = "Data Saved Correctly";
                    FillGridView();
                }
                else
                {
                    StatusLbl.ForeColor = System.Drawing.Color.Red;
                    StatusLbl.Text = "Data Not Saved Contact to your Admin";
                }

            }
            else
            {
                StatusLbl.ForeColor = System.Drawing.Color.Red;
                StatusLbl.Text = "Data Not Saved";

            }

        }

        protected void ButtonUpdate_Click1(object sender, EventArgs e)
        {
            /* find id by hiddenfield */
            Admin admin = new Admin();
            admin.AdminId = Convert.ToInt32(hiddenfield1.Value);

            admin.Region = RegionDrop.SelectedItem.Text;
            admin.City = CityDrop.SelectedItem.Text;
            admin.Price = Convert.ToInt32(PriceDrop.SelectedItem.Text);
            admin.Rooms = Convert.ToInt32(RoomDrop.SelectedItem.Text);
            admin.Bathrooms = Convert.ToInt32(BathroomDrop.SelectedItem.Text);
            admin.Garage = GarageDrop.SelectedItem.Text;
            admin.Pool = PoolDrop.SelectedItem.Text;
            admin.Fireplace = FireplaceDrop.SelectedItem.Text;

            /* call the function UpdateCustomer from the CustomerDB */
            bool result = admin.UpdateAdmin(admin);
            if (result)
            {
                StatusLbl.Text = "Update has done successfully!";
                /* call the funtion FillGridView(connect to FridView) */
                FillGridView();
            }
            else
            {
                StatusLbl.Text = "Unable to update!";
            }
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            /* find id by hiddenfield */
            Admin admin = new Admin();
            int id = Convert.ToInt32(hiddenfield1.Value);

            /* call the function DeleteCustomer from the DB */
            bool result =admin.DeleteAdmin(id);
            if (result)
            {
                StatusLbl.Text = "Delete has done successfully!";
                FillGridView();
            }
            else
            {
                StatusLbl.Text = "Delete has not done successfully!";
            }

        }
    }
}