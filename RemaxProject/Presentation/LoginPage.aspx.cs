﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RemaxProject.Models;

namespace RemaxProject.Presentation
{
    public partial class LoginPage : System.Web.UI.Page
    {
        /* connect to database */
        public static string cn = ConfigurationManager.ConnectionStrings["Context"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                User user = new User();
                user.Email = TextBoxEmail.Text;
                user.Password = TextBoxPassword.Text;

                /* call the function SaveCustomer from the CustomerDB */
                if (user.SaveUser(user))
                {
                    LabelMessage.ForeColor = System.Drawing.Color.Green;
                    LabelMessage.Text = "Data Saved Correctly";

                }
                else
                {
                    LabelMessage.ForeColor = System.Drawing.Color.Red;
                    LabelMessage.Text = "Data Not Saved Contact to your Admin";
                }

            }
            else
            {
                LabelMessage.ForeColor = System.Drawing.Color.Red;
                LabelMessage.Text = "Data Not Saved";

            }

        }
    }
}