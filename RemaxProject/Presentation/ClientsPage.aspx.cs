﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RemaxProject.Models;


namespace RemaxProject.Presentation
{
    public partial class AdminPage : System.Web.UI.Page
    {
        //public static string cn = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            Client client = new Client();
            GridViewListClient.DataSource = client.ReadClient();
            GridViewListClient.DataBind();

            /* call GridView function to read data */
            FillGridView();
        }

        public void FillGridView()
        {
            Client client = new Client();
            var dt = client.ReadClient();
            GridViewListClient.DataSource = dt;
            GridViewListClient.DataBind();
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            string cn = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            SqlConnection con = new SqlConnection(cn);
            con.Open();
            SqlCommand cmd = new SqlCommand("Select * FROM Client WHERE CliendId=@TextBoxSearchById", con);

            try
            {

                SqlParameter search = new SqlParameter();
                search.ParameterName = "@TextBoxSearchById";
                search.Value = TextBoxSearchById.Text.Trim();

                cmd.Parameters.Add(search);
                SqlDataReader dr = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(dr);

                GridViewListClient.DataSource = dt;
                GridViewListClient.DataBind();
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }
    }
}