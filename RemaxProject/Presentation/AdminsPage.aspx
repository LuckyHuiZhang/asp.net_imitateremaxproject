﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminsPage.aspx.cs" Inherits="RemaxProject.Presentation.ClientPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ClientPage</title>
    <%--add reference and resource--%>
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

     <%--add style--%>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1" />
    <style>
    .container {
          margin: auto;
          width: auto;
          border: 3px solid #73AD21;
          padding: 10px;
          font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
     }
        .auto-style1 {
            position: relative;
            width: 100%;
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
            left: 0px;
            top: 0px;
            padding-left: 15px;
            padding-right: 15px;
        }
     </style>

</head>
<body>
    <form id="form1" runat="server">

 <div class="container" style="background-color: #ffbf00;">
      <%--add HiddenField to get customer's ID--%>
            <asp:HiddenField ID="hiddenfield1" runat="server"/>
    <div class="form-row">
        <div class="form-group col-md-3">
          <label for="inputState">Region</label>
          <asp:DropDownList ID="RegionDrop" runat="server">
                <asp:ListItem Value="1">Quebec</asp:ListItem>
                <asp:ListItem Value="2">Ontario</asp:ListItem>
                <asp:ListItem Value="3">British Columbia</asp:ListItem>
           </asp:DropDownList>
        </div>
        <div class="form-group col-md-3">
          <label for="inputState">City</label>
          <asp:DropDownList ID="CityDrop" runat="server">
                 <asp:ListItem Value="1">Montreal</asp:ListItem>
                 <asp:ListItem Value="2">Toronto</asp:ListItem>
                 <asp:ListItem Value="3">Vancouver</asp:ListItem>
           </asp:DropDownList>
        </div>
        <div class="form-group col-md-3">
          <label for="inputState">Price</label>
         <asp:DropDownList ID="PriceDrop" runat="server">
                 <asp:ListItem Value="1">100,000</asp:ListItem>
                 <asp:ListItem Value="2">150,000</asp:ListItem>
                 <asp:ListItem Value="3">200,000</asp:ListItem>
           </asp:DropDownList>
        </div>
        <div class="form-group col-md-3">
          <label for="inputState">Room(s)</label>
          <asp:DropDownList ID="RoomDrop" runat="server">
                 <asp:ListItem Value="1">3</asp:ListItem>
                 <asp:ListItem Value="2">2</asp:ListItem>
                 <asp:ListItem Value="3">1</asp:ListItem>
           </asp:DropDownList>
        </div>   
    </div>    

    <div class="form-row">
        <div class="form-group col-md-3">
          <label for="inputState">Bathrooms</label>
          <asp:DropDownList ID="BathroomDrop" runat="server">
                 <asp:ListItem Value="1">3</asp:ListItem>
                 <asp:ListItem Value="2">2</asp:ListItem>
                 <asp:ListItem Value="3">1</asp:ListItem>
           </asp:DropDownList>
        </div>
        <div class="form-group col-md-3">
          <label for="inputState">Garage</label>
          <asp:DropDownList ID="GarageDrop" runat="server">
                 <asp:ListItem Value="1">yes</asp:ListItem>
                 <asp:ListItem Value="2">no</asp:ListItem>
           </asp:DropDownList>
        </div>
        <div class="form-group col-md-3">
          <label for="inputState">Pool</label>
             <asp:DropDownList ID="PoolDrop" runat="server">
                 <asp:ListItem Value="1">yes</asp:ListItem>
                 <asp:ListItem Value="2">no</asp:ListItem>
           </asp:DropDownList>
        </div>
        <div class="form-group col-md-3">
          <label for="inputState">Fireplace</label>
           <asp:DropDownList ID="FireplaceDrop" runat="server">
                 <asp:ListItem Value="1">yes</asp:ListItem>
                 <asp:ListItem Value="2">no</asp:ListItem>
           </asp:DropDownList>
            <br />         
            <asp:Label ID="StatusLbl" runat="server" Text=""></asp:Label>
        </div>   
    </div>    
     
     <div class="form-group row ">
            <div class="col-sm-3">
                   <br />
                   <%-- <button type="submit" class="btn btn-primary m-1">Save</button>--%>
                  <asp:Button ID="ButtonSave" runat="server" Text="Save" class="btn btn-primary" OnClick="ButtonSave_Click1"  />
            </div>
            <div class="col-sm-3">
                    <br />
                    <%-- <button type="submit" class="btn btn-primary m-1">Update</button>--%>
                 <asp:Button ID="ButtonUpdate" runat="server" Text="Update" class="btn btn-primary" OnClick="ButtonUpdate_Click1"  />
            </div>
            <div class="col-sm-3">
                    <br />
                    <%--  <button type="submit" class="btn btn-primary m-1">Delete</button>--%>
                 <asp:Button ID="ButtonDelete" runat="server" Text="Delete" class="btn btn-primary" OnClick="ButtonDelete_Click"  />
            </div>
      </div>  
           
     <div>
         <asp:GridView ID="GridViewListAdmin" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="false">
             <AlternatingRowStyle BackColor="White" />
             <EditRowStyle BackColor="#7C6F57" />
             <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
             <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
             <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
             <RowStyle BackColor="#E3EAEB" />
             <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
             <SortedAscendingCellStyle BackColor="#F8FAFA" />
             <SortedAscendingHeaderStyle BackColor="#246B61" />
             <SortedDescendingCellStyle BackColor="#D4DFE1" />
             <SortedDescendingHeaderStyle BackColor="#15524A" />
             <Columns>
                     <asp:BoundField DataField="AdminId" HeaderText="AdminId" SortExpression="AdminId" />
                     <asp:BoundField DataField="Region" HeaderText="Region" SortExpression="Region" />
                     <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
                     <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" />
                     <asp:BoundField DataField="Rooms" HeaderText="Rooms" SortExpression="Rooms" />
                     <asp:BoundField DataField="Bathrooms" HeaderText="Bathrooms" SortExpression="Bathrooms" />
                     <asp:BoundField DataField="Garage" HeaderText="Garage" SortExpression="Garage" />
                     <asp:BoundField DataField="Pool" HeaderText="Pool" SortExpression="Pool" />
                     <asp:BoundField DataField="Fireplace" HeaderText="Fireplace" SortExpression="Fireplace" />
                   <%-- add TemplateField(allows for a higher degree of flexibility in displaying data than is available with the other field controls)--%>
                     <asp:TemplateField>
                         <ItemTemplate>
                             <asp:LinkButton runat="server" CausesValidation="false" CommandArgument='<%# Eval("AdminId") %>'>View</asp:LinkButton>
                         </ItemTemplate>
                     </asp:TemplateField>
                 </Columns>      
         </asp:GridView>
     </div>
</div>
    </form>
</body>
</html>
