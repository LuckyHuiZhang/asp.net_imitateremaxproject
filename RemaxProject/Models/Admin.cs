﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data;
using RemaxProject.DataAccess;
using System.ComponentModel.DataAnnotations.Schema;

namespace RemaxProject.Models
{
    //map model to our table 
    [Table("Admin")]
    public class Admin
    {
        public int AdminId { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public int Price { get; set; }
        public int Rooms { get; set; }
        public int Bathrooms { get; set; }
        public string Garage { get; set; }
        public string Pool { get; set; }
        public string Fireplace { get; set; }
        public byte[] Image { get; set; }

        public DataTable ReadAdmin()
        {
            return AdminDB.ReadAdmin();
        }
        public bool SavAdmin(Admin admin)
        {
            return AdminDB.SaveAdmin(admin);
        }

        public bool DeleteAdmin(int id)
        {
            return AdminDB.DeleteAdmin(id);
        }

        public Admin FindAdmin(int id)
        {
            return AdminDB.FindAdmin(id);
        }
        public bool UpdateAdmin(Admin admin)
        {
            return AdminDB.UpdateAdmin(admin);
        }
    }
}