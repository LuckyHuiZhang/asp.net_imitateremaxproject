﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data;
using RemaxProject.DataAccess;

namespace RemaxProject.Models
{
    [Table("User")]
    public class User
    {
        public int UserId { get; set; }

        [Required]
        public string Email { get; set; }
        public string Password { get; set; }

        public DataTable ReadUser()
        {
            return UserDB.ReadUser();
        }
        public bool SaveUser(User user)
        {
            return UserDB.SaveUser(user);
        }
    }
}