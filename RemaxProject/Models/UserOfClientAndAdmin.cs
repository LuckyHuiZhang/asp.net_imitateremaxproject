﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RemaxProject.Models
{
    public class UserOfClientAndAdmin
    {
        public List<User> ListUser { get; set; }
        public List<Client> ListClient{ get; set; }
        public List<Admin> ListAdmin { get; set; }

    }
}