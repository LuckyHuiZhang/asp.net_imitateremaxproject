﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data;
using RemaxProject.DataAccess;

namespace RemaxProject.Models
{
    //map our model to our table 
    [Table("Client")]
    public class Client
    {
        [Required]
        public int ClientId { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public int Price { get; set; }
        public int Rooms { get; set; }
        public int Bathrooms { get; set; }
        public string Garage { get; set; }
        public string Pool { get; set; }
        public string Fireplace { get; set; }

        public DataTable ReadClient()
        {
            return ClientDB.ReadClient();
        }   

        public Client FindClient(int id)
        {
            return ClientDB.FindClient(id);
        }
      

    }
}