﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RemaxProject.Models;

namespace RemaxProject.DataAccess
{
    public static class ClientDB
    {
        /* connect to database */
        public static string cn = ConfigurationManager.ConnectionStrings["Context"].ConnectionString;

        public static DataTable ReadClient()
        {
            using (SqlConnection con = new SqlConnection(cn))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "Select * from client";
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                DataTable dt = new DataTable();
                dt.Load(reader);
                reader.Close();
                return dt;

            }
        }
        
        public static Client FindClient(int id)
        {
            Client client = new Client();
            using (SqlConnection con = new SqlConnection(cn))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "Select * from client where Id=" + id;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    client.ClientId = Convert.ToInt32(reader["ClientId"]);
                    client.Region = reader["Region"].ToString();
                    client.City = reader["City"].ToString();
                    client.Price = Convert.ToInt32(reader["Price"]);
                    client.Rooms = Convert.ToInt32(reader["Rooms"]);
                    client.Bathrooms = Convert.ToInt32(reader["Bathrooms"]);
                    client.Garage = reader["Garage"].ToString();
                    client.Pool = reader["Pool"].ToString();
                    client.Fireplace = reader["Fireplace"].ToString();
                }
                reader.Close();
            }
            return client;
        }

      

    }
}