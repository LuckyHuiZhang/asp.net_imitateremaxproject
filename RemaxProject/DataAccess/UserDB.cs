﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RemaxProject.Models;

namespace RemaxProject.DataAccess
{
    public static class UserDB
    {
        /* connect to database */
        public static string cn = ConfigurationManager.ConnectionStrings["Context"].ConnectionString;

        public static DataTable ReadUser()
        {
            using (SqlConnection con = new SqlConnection(cn))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "Select * from user";
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                DataTable dt = new DataTable();
                dt.Load(reader);
                reader.Close();
                return dt;

            }
        }
        public static bool SaveUser(User user)
        {
            bool result = true;
            try
            {
                using (SqlConnection con = new SqlConnection(cn))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandText = string.Format("insert into User (Email , Password) values ('{0}','{1}')",
                    user.Email, user.Password);
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {

                result = false;

            }
            return result;
        }

    }
}