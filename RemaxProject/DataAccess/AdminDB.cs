﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RemaxProject.Models;

namespace RemaxProject.DataAccess
{
    public class AdminDB
    {
        /* connect to database */
        public static string cn = ConfigurationManager.ConnectionStrings["Context"].ConnectionString;

        public static DataTable ReadAdmin()
        {
            using (SqlConnection con = new SqlConnection(cn))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "Select * from admin";
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                DataTable dt = new DataTable();
                dt.Load(reader);
                reader.Close();
                return dt;

            }
        }
        public static bool SaveAdmin(Admin admin)
        {
            bool result = true;
            try
            {
                using (SqlConnection con = new SqlConnection(cn))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandText = string.Format("insert into admin (Region, City, Price, Rooms, Bathrooms, Garage, Pool, Fireplac, image) values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')",
                    admin.Region, admin.City, admin.Price, admin.Rooms, admin.Bathrooms, admin.Garage, admin.Pool, admin.Fireplace, admin.Image);
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {

                result = false;

            }
            return result;

        }

        public static bool DeleteAdmin(int id)
        {
            bool result = true;
            try
            {
                using (SqlConnection con = new SqlConnection(cn))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandText = "delete from admin where AdminId= " + id;
                    con.Open();
                    cmd.ExecuteNonQuery();
                }

            }
            catch (Exception)
            {
                result = false;
            }
            return result;

        }

        public static Admin FindAdmin(int id)
        {
            Admin admin = new Admin();
            using (SqlConnection con = new SqlConnection(cn))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "Select * from admin where AdminId=" + id;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    admin.AdminId = Convert.ToInt32(reader["AdminId"]);
                    admin.Region = reader["Region"].ToString();
                    admin.City = reader["City"].ToString();
                    admin.Price = Convert.ToInt32(reader["Price"]);
                    admin.Rooms = Convert.ToInt32(reader["Rooms"]);
                    admin.Bathrooms = Convert.ToInt32(reader["Bathrooms"]);
                    admin.Garage = reader["Garage"].ToString();
                    admin.Pool = reader["Pool"].ToString();
                    admin.Fireplace = reader["Fireplace"].ToString();
                }
                reader.Close();
            }
            return admin;
        }

        public static bool UpdateAdmin(Admin admin)
        {
            bool result = true;
            try
            {
                using (SqlConnection con = new SqlConnection(cn))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandText = string.Format("update client set Region='{0}', City='{1}', Price={2}, Rooms='{3}', Bathrooms='{4}', Garage='{5}' , Pool='{6}', Fireplace='{7}' where Id={8}",
                    admin.Region, admin.City, admin.Price, admin.Rooms, admin.Bathrooms, admin.Garage, admin.Pool, admin.Fireplace, admin.AdminId);
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                var j = e.Message.ToString();
                result = false;
                throw;
            }

            return result;

        }

    }
}